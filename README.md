# Predicción del precio de viviendas ubicadas en estados costeros de Estados Unidos. 
Este documento presenta la decripción de la solución, la arquitectura y las principales consideraciones y pasos requeridos para realizar el despliegue e instalación para la Predicción del precio de viviendas ubicadas en estados costeros de Estados Unidos.

Se quiere resolver un problema práctico de predicción utilizando las herramientas estadísticas y de Machine Learning y los conocimientos adquiridos en las materias del semestre. Empleando el precio histórico (series de tiempo) de las viviendas costeras en Estados Unidos, se explorarán modelos autorregresivos (ARIMA) y compararán con Deep Learning (RNN-LSTM).


## Tabla de Contenidos
* [Descripción de la solución](#descripción-de-la-solución)
* [Proceso de ejecucion y despliegue](#Proceso de ejecucion y despliegue)
* [Requerimientos](#requerimientos)
* [Instalacion](#instalación)
* [Autores](#autores)


## Descripción de la solución
**Objetivo general**



**Objetivos específicos**
 

### Entregables




### Preguntas de Investigación Esenciales:


### Impacto potencial esperado en el Negocio


## Arquitectura de la solución


## Estructura del proyecto


### Solución 

## Proceso de ejecucion y despliegue

### Parte 1: Generacion de Vista Minable y Modelos

 



### Parte 2: Visualización



### Librerias Empleadas 

pandas\
numpy\
seaborn\
matplotlib\
datetime\
pickle\
sklearn\
scipy\
Darts\
Xarray\


## Requerimientos

### Requerimientos Software

A continuación, se presentan los requerimientos correspondientes de software para el despliegue de la aplicación desarrollada.

| Requerimiento   |           |
|----------|-------------|
| Python |  >=3.4 |
| Sistema Operativo |  Ubuntu |
| Contenedor de software | Docker 18.06.3|
| Consolas virtuales | Screen 4.00|
| Sistemas de control de versiones | Git 1.8.3|
|Power BI | Versión 2.94.921.0 64-bit  (Junio de 2021)|

### Requerimientos Hardware

A continuación, se presentan los requerimientos correspondientes de hardware para el despliegue de la aplicación desarrollada.Para un óptimo procesamiento de datos y para una buena ejecución del análisis de modelos de clúster se recomienda un equipo con las siguientes características:

| Requerimiento   |           |
|----------|-------------|
| Memoria RAM |  16 GB |
| Almacenamiento |    500 GB   |
| CPU | Cantidad de núcleos 16, cantidad de subprocesos 32, frecuencia básica del procesador 2.30 GHz|


## Instalación: 
Instalar anaconda para utilizar Jupyter notebook y poder acceder a los archivos ipynb u otra alternativa es utilizarlos en google colab (tener cuenta gmail)

https://docs.anaconda.com/anaconda/install/ 

## Proceso de instalacion de ambiente del proyecto

`conda create -n proy-prediccion python=3.8`

`conda activate proy-predicción`

`conda install --file requirements.txt`

## Autores

| Organización   | Nombre del Miembro | Correo electronico | 
|----------|-------------|-------------|

|
| Universidad Eafit  |  Cristian David Rojas Rincón: Cientific@ de Datos  |  cdrojasr@eafit.edu.co | Economista |
| Universidad Eafit  |  Daniela Vasquez Jaramillo: Cientific@ de Datos  |  dvasqu18@eafit.edu.co | Negociadora Internacional|
| Universidad Eafit  |  David Rua Jaramillo: Cientific@ de Datos  |  druaj@eafit.edu.co | Administrado de negocios|
| Universidad Eafit  |  Joé Carlos Díaz M.: Cientific@ de Datos  |  jcdiazm1@eafit.edu.co | Ingeniero Civil|


