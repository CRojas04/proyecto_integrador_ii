# CAF Sistema Nacional de Cuidado

Este documento presenta la descripción de la solución, la arquitectura y las principales consideraciones y pasos requeridos para realizar el despliegue e instalación del proyecto de CAF Sistema Nacional de Cuidado.

Tambien, en el siguientes links se encuentran:

[Carpeta de Drive asociada al proyecto:](https://drive.google.com/drive/folders/13XjS9dSyEoYTz6LozOzY7TB8N1oFpHYk?usp=sharing)

[Informe Proyecto](https://drive.google.com/file/d/14u-E1VUvZrF1_fXtFGaUAT_YmUkEEfAF/view?usp=sharing)

[Manual de Usuario](https://drive.google.com/file/d/1OKICLQogJSLUQKvvH3Gp-U1p5ANhdtJQ/view?usp=sharing)


## Descripción de la solución

### Objetivo General: 
Describir y pronosticar la demanda y el costo para el componente de salud de las atenciones relacionadas con el cuidado para la población de la niñez, adultos mayores y adultos con discapacidad cuyo diagnóstico requiere de cuidado.

### Objetivo Específicos:
a. Determinar los diagnósticos relacionados para considerar en el estudio.

b. Caracterizar las poblaciones vulnerables impactadas por Covid-19 que requieren labores de cuidado dada las patologías y comorbilidades existentes.

c. Describir y comparar los costos de los servicios de cuidado diferenciados por régimen (subsidiado y contributivo).

d. Describir la demanda de las atenciones relacionadas con el cuidado por regiones.

e. Pronosticar la demanda y el costo de las atenciones relacionadas con el cuidado para las poblaciones y regiones definidas 

### Entregables

**Producto 1:** Reporte técnico con los diagnósticos relacionados para considerar en el estudio, específicamente los que se relacionan con labores de cuidado en el componente de salud.

**Producto 2:** Dashboard en PowerBI, acorde a los requisitos de visualización del DNP y que integre y comunique los resultados de:

-Caracterizar las poblaciones vulnerables impactadas por Covid-19 que requieren labores de
cuidado dada las patologías y comorbilidades existentes.

-Describir y comparar los costos y la demanda de los servicios de cuidado diferenciados por régimen (subsidiado y contributivo).

-Describir los costos y la demanda de las atenciones relacionadas con el cuidado por regiones a
nivel de municipio.

Se dejará como entrada de la preparación de los datos para este producto un archivo de
configuración que permita introducir los códigos CIE10 (ICD10) que serán caracterizados.

**Producto 3:** Modelo de pronóstico de la demanda y el costo del cuidador para poblaciones caracterizadas.

**Producto 4:** Reporte técnico de herramienta desarrollada

### Preguntas de Investigación Esenciales:

1. Modelos de analítica útiles para estimación de costos de servicios de salud
2. Modelos de analítica útiles  para estimación de demanda de servicios de salud

## Arquitectura de la solución

![](docs/docs_Arquitectura.png)

## Estructura del proyecto

```
caf-snc
│   README.md
│   requirements.txt  
│   scholar.log 
└───datalab
│   └───state-of-art
│   └───consolidado-CIE10
│   └───recobros
│   └───rips
│   └───suficiencia
└───project
│   root.py
│   └───config
│       │   recobros.conf
│   └───logic
│       │   dataload
│       │   datareport
│       │   models
│       │   utils
│   └───run
└───dashboards
│   00dashboardv1.pbix
│   01versionPreEntrega.pbix
│   02versionEntrega.pbix
│   colombia-municipios.json
│   └───background
│   └───logos
│   └───prediccion
│   └───view_v3_year
│   └───view_v3_yearmonth
│   └───z-score
└───docs

```
## Proceso de ejecucion y despliegue

### Parte 1: Generacion de Vista Minable y Modelos

La ejecución del proyecto consiste en diferentes pasos que conllevan:

- Paso 1: Clase de DataLoad en archivo project/logic/dataload es instanciada por las otras clases
- Paso 2: Se ejecutan los mining views de RIPS, de Recobros y de Suficiencia
```
    ipython project/logic/dataload/rips_mining_view.py
    ipython project/logic/dataload/recobros_mining_view.py
    ipython project/logic/dataload/suficiencia_mining_view.py
```
- Paso 3.1: Se ejecuta el modelo de series de tiempo
```
    ipython project/logic/models/timeseries/timeseries_testing.py
```
- Paso 3.2: Se ejecuta el jupyter notebook de la comparacion de suficiencia y rips
```
   datalab/rips/compare.ipynb
```
- Paso 3.3: Se ejecuta el jupyter notebook del calculo de costos de cuidado
```
    datalab/recobros/costos.ipynb
```

### Parte 2: Visualización

![](docs/docs_modeloDatosPowerBI.png)

El modelo de datos de PowerBI presentado indica las relaciones entre las diferentes tablas (las cuales tienen el prefijo ​Hechos_​) que consumen las diferentes vistas. En amarillo se indican los tablas de los conteos por Año y por Año-Mes para las vista de análisis de RIPS y Estructura Poblacional, en azul la tabla de Z-score para la vista de análisis geografico y en rojo la tabla de la vista de predicción de demanda. Mientras, la tabla denominada ​Dim_Location​ ​es transversal para las diferentes vistas y tiene el objetivo de auxiliar tanto en los filtros aplicados (especialmente los de Entidad Territoriales) y la visualización de comparación geográfica (Figura 1).

- Paso para obtener **archivos de soporte para visualización de PowerBI**: Se ejecuta la clase de **DashboardMetrics** para obtener archivo que soporta las tablas *Dim_Location* y *Hechos_ZscoresByYear*
```
    ipython caf-snc/project/logic/dataload/dashboard_metrics.py
```

## Requerimientos

### Requerimientos Software

A continuación, se presentan los requerimientos correspondientes de software para el despliegue de la aplicación desarrollada.

| Requerimiento   |           |
|----------|-------------|
| Python |  >=3.7 |
| Sistema Operativo |  Centos / RHEL 7.6 |
| Contenedor de software | Docker 18.06.3|
| Consolas virtuales | Screen 4.00|
| Sistemas de control de versiones | Git 1.8.3|
|Power BI | Versión2.86.902.0 64-bit  (octubre de 2020)|

### Requerimientos Hardware

A continuación, se presentan los requerimientos correspondientes de hardware para el despliegue de la aplicación desarrollada.

| Requerimiento   |           |
|----------|-------------|
| Memoria RAM |  16 GB |
| Almacenamiento |    100 GB   |
| CPU | Cantidad de núcleos 16, cantidad de subprocesos 32, frecuencia básica del procesador 2.30 GHz|


## Autores

| Organización   | Nombre del Miembro | Usuarioa de GitLab |
|----------|-------------|-------------|
| Alianza CAOBA |  Andres Moreno: Lider del proyecto | @andres.moreno.barbosa |
| Alianza CAOBA |  Jose Francisco | @jf.molano |
| Alianza CAOBA |  Camila Martinez | @kamynz16 |
| DNP  |Jairo Tirado|
| DNP  |David Aguilar| 
| DNP  |Nicolás Agudelo|
| MinSalud|Felix Nates | 
