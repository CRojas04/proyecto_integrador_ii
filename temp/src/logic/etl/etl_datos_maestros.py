# Funciones para extraer los archivos entregados por Nutresa

import glob
import os
import re

import pandas as pd

import logic.utils.funciones_lectura as lectura
# Importa las funciones
from logic.utils.funciones_etl import cargar_excel, clean, limpiar_nombre_municipio, extraer_tipo, tipos, construir_nit


def extraer_dm(ubicacion):
    '''
    Método que extrae los datos maestros de los clientes a partir de los archivos crudos.

    Cambios a la fuente:
    Carga la fuente como viene (ignorando lineas corruptas).
    Selecciona las columnas relevantes
    Limpia y renombra el campo KUNNR a id_cliente

    Parámetros
    ----------
    ubicacion : string
            Ubicación de la carpeta donde se encuentran los archivos crudos

    Devuelve
    ---------
    pd.DataFrame
            pd.DataFrame con los valores de datos maestros clientes
    '''

    cols = ['KUNNR', 'SORTL', 'STCD1', 'NAME1',
            'NAME2', 'ORT01', 'REGIO', 'FITYP','ERDAT','STCDT']
    print('Start', ubicacion )
    files = glob.glob(ubicacion + "/DM_*_*.csv")
    print('End', ubicacion)
    p = re.compile(".*DM_\d_\d{8}\.csv")
    files = [f for f in files if p.match(f)]
    print(f'Archivos datos maestros a cargar son: {files}')

    dataframes_load = [pd.read_csv(i, low_memory=False, sep='\t',
                                   encoding='utf-16', skiprows=1, usecols=cols + ['STRAS'], error_bad_lines=False) for i
                       in files]

    # Archivo 3
    dm_3 = cargar_excel(os.path.join(
        ubicacion, 'DM_4_CLIENTES.XLSX'), columnas=cols)

    # Concatena
    dm = pd.concat((pd.concat(dataframes_load, ignore_index=True), dm_3), ignore_index=True).rename(
        columns={'KUNNR': 'id_cliente'})

    # Arregla los ids
    dm.id_cliente = dm.id_cliente.apply(clean)

    # Vuelve strings las columnas
    dm.SORTL = dm.SORTL.apply(clean)
    dm.STCD1 = dm.STCD1.apply(clean)

    # Pega los valores geograficos
    # Extrae la base de datos de municipios
    df_muni = pd.read_csv(os.path.join(ubicacion, 'Departamentos_y_municipios_de_Colombia.csv'))

    # Selecciona las columnas
    df_muni = df_muni[['CÓDIGO DANE DEL DEPARTAMENTO', 'CÓDIGO DANE DEL MUNICIPIO', 'DEPARTAMENTO', 'MUNICIPIO']].copy()
    df_muni.columns = ['cod_dept', 'cod_muni', 'depto_name', 'muni_name']

    # Limpia
    df_muni.cod_dept = pd.to_numeric(df_muni.cod_dept, errors='coerce').fillna(-1).astype(int)
    df_muni.cod_muni = df_muni.cod_muni.apply(lambda s: s.replace(',', '')).fillna(-1).astype(int)
    df_muni.depto_name = df_muni.depto_name.apply(limpiar_nombre_municipio)
    df_muni.muni_name = df_muni.muni_name.apply(limpiar_nombre_municipio)

    # Agrega las variables para el cruce
    dm['muni_name'] = dm['ORT01'].apply(limpiar_nombre_municipio)
    dm['cod_dept'] = pd.to_numeric(dm['REGIO'], errors='coerce').fillna(-1).astype(int)

    # Cruza
    dm = dm.merge(df_muni, on=['cod_dept', 'muni_name'], how='left')

    # limpia
    dm.cod_muni = dm.cod_muni.fillna(-1).astype(int)
    dm.loc[dm.muni_name == 'Bogota', 'depto_name'] = 'Bogota DC'
    dm.loc[dm.muni_name == 'Bogota', 'cod_muni'] = 11001

    return dm


def extraer_dm_ov(ubicacion):
    '''
    Método que extrae los datos maestros de los clientes (organización de ventas) a partir de los archivos crudos.

    Cambios a la fuente:
    Carga la fuente como viene (ignorando lineas corruptas).
    Selecciona las columnas relevantes
    Limpia y renombra el campo '/BIC/ZSDCUSTOM' a id_cliente
    Descarta los registros que no son clientes. La definición de cliente del proyecto es:
            - Que tenga oficina de ventas (SALES_OFF != Nulo)
            - Que su id cliente empiece por 1,2 o 4.
    Agrega la columna 'tipo' con el primer digito del id_cliente
    Agrega la columna 'activo' indicando si es activo a la fecha de entrega de los datos
    
    Parámetros
    ----------
    ubicacion : string
            Ubicación de la carpeta donde se encuentran los archivos crudos

    Devuelve
    ---------
    pd.DataFrame
            pd.DataFrame con los valores  relevantes de datos maestros (organización de ventas) clientes, filtrados
            por la definición de cliente
    '''

    # Las columnas relevantes a importar
    cols = ['/BIC/ZCLIENACT', '/BIC/ZSDCUSTOM',
            'CUST_GROUP', 'CUST_GRP1', 'DISTR_CHAN', 'SALES_OFF']

    # , 'DM_OV_3.XLSX','DM_OV_2.XLSX', 'DM_OV_1.XLSX']
    archivos = glob.glob(ubicacion + "/DM_OV_*.XLSX")
    p = re.compile(r".*DM_OV_\d{8}\.XLSX")
    archivos = [f for f in archivos if p.match(f)]
    print(f'Archivos organización de ventas a cargar son: {archivos}')

    frames = []
    for archivo in archivos:
        # print('Extrayendo: {}'.format(archivo))
        # Importa grupo 1 (solo los que tienen oficinas de ventas)
        dm_ov_temp = pd.read_excel(archivo, usecols=cols).dropna(subset=["SALES_OFF"])
        # Mira si es activo
        dm_ov_temp['activo'] = dm_ov_temp['/BIC/ZCLIENACT'] != 'X'
        dm_ov_temp = dm_ov_temp[['/BIC/ZSDCUSTOM', 'CUST_GROUP', 'CUST_GRP1',
                                 'DISTR_CHAN', 'activo']].rename(columns={'/BIC/ZSDCUSTOM': 'id_cliente'})

        # Limpia el ID y extrae el tipo
        dm_ov_temp.id_cliente = dm_ov_temp.id_cliente.apply(clean)
        dm_ov_temp['tipo'] = dm_ov_temp.id_cliente.apply(extraer_tipo)

        # Filtra por el tipo de cliente
        dm_ov_temp = dm_ov_temp[dm_ov_temp.tipo.isin(tipos)]

        frames.append(dm_ov_temp)

    dm_ov = pd.concat(frames, ignore_index=True)

    return dm_ov


def extraer_geo(ubicacion):
    '''
    Extraer los datos geográficos los archivos crudos

    Cambios a la fuente:
    Carga la fuente como viene (ignorando lineas corruptas).
    Selecciona las columnas relevantes
    Limpia y renombra el campo 'Cod_Cliente' a id_cliente

    Parámetros
    ----------
    ubicacion : string
            Ubicación de la carpeta donde se encuentran los archivos crudos

    Devuelve
    ---------
    pd.DataFrame
            pd.DataFrame con los valores relevantes de los datos geográficos

    '''
    # Archivo
    archivos = glob.glob(ubicacion + "/GEO_CLIENTES_ZDNA*.csv")

    print(f'Archivo datos geográficos a cargar es : {archivos[0]}')

    # Columnas Relevantes
    cols = ['Cod_Cliente', 'Point_x', 'Point_Y']

    # Lee el archivo
    geo = pd.read_csv(archivos[0], sep=';', encoding='ISO-8859-1',
                      usecols=cols).rename(columns={'Cod_Cliente': 'id_cliente'})

    # Limpia el id
    geo.id_cliente = geo.id_cliente.apply(clean)

    return (geo)


def procesar(ubicacion_entrada, ubicacion_salida, archivo_configuraciones):
    '''
    Método principal
    '''

    # Ejecuta el Pipeline
    print('Inicia el ETL Para Datos Maestros')

    # Datos Maestros
    print('Datos Maestros')
    print('   Extrae')
    dm = extraer_dm(ubicacion_entrada)
    dm_ov = extraer_dm_ov(ubicacion_entrada)
    geo = extraer_geo(ubicacion_entrada)

    # Los junta
    print('   Junta')
    dm_final = dm.merge(dm_ov, on='id_cliente')
    dm_final = dm_final.merge(geo, on='id_cliente', how='left')

    # Asigna el NIT
    print('   Asigna NIT')
    dm_final = dm_final.merge(dm_final.apply(lambda row: pd.Series(dict(zip(
        ['NIT', 'certidumbre'], construir_nit(row.STCD1, row.SORTL)))), axis=1), left_index=True, right_index=True)
    print('   Limpia')

    # Limpia nombres de municipios

    # Limpiamos el nombre del municipio
    dm_final['ORT01'] = dm_final.apply(lambda x: limpiar_nombre_municipio(x.ORT01), axis=1)

    # Ajustamos errores de la fuente de datos maestros en nombre de municipios mayores a 30 registros
    dm_final['ORT01'] = dm_final['ORT01'].replace(['Cucuta'], 'Cúcuta')
    dm_final['ORT01'] = dm_final['ORT01'].replace(['El Retiro'], 'Retiro')
    dm_final['ORT01'] = dm_final['ORT01'].replace(['Anzá'], 'Anza')
    dm_final['ORT01'] = dm_final['ORT01'].replace(['Santa Fé De Antioquia'], 'Santafé De Antioquia')
    dm_final['ORT01'] = dm_final['ORT01'].replace(['Calarca'], 'Calarcá')
    dm_final['ORT01'] = dm_final['ORT01'].replace(['Villa Pinzón'], 'Villapinzón')
    dm_final['ORT01'] = dm_final['ORT01'].replace(['Sta Rosa Sur'], 'Santa Rosa')
    dm_final['ORT01'] = dm_final['ORT01'].replace(['Mompos'], 'Mompós')
    dm_final['ORT01'] = dm_final['ORT01'].replace(['Tumaco'], 'San Andrés De Tumaco')
    dm_final['ORT01'] = dm_final['ORT01'].replace(['Soata'], 'Soatá')
    dm_final['ORT01'] = dm_final['ORT01'].replace(['Villa Rosario'], 'Villa Del Rosario')
    dm_final['ORT01'] = dm_final['ORT01'].replace(['Vistahermosa'], 'Vista Hermosa')
    dm_final['ORT01'] = dm_final['ORT01'].replace(['Acacías'], 'Acacias')
    dm_final['ORT01'] = dm_final['ORT01'].replace(['San Juan De Rioseco'], 'San Juan De Río Seco')
    dm_final['ORT01'] = dm_final['ORT01'].replace(['Magangue'], 'Magangué')
    dm_final['ORT01'] = dm_final['ORT01'].replace(['Ariguaní El Difícil'], 'Ariguaní')
    dm_final['ORT01'] = dm_final['ORT01'].replace(['Piéndamo'], 'Piendamó')
    dm_final['ORT01'] = dm_final['ORT01'].replace(['Bogotá'], 'Bogotá D.C.')
    print("Corrección de inconsistencias en nombres de municipios de la fuente de datos maestros")

    print('   Guarda')
    dm_final.to_pickle(os.path.join(
        ubicacion_salida, 'datos_maestros_final.pkl'))

    print('Acabo')


# Ejecuta el pipeline
if __name__ == "__main__":
    variables = lectura.extraer_parametros()
    ubicacion_entrada, ubicacion_salida, archivo_configuraciones = variables['ubicacion_entrada'], variables[
        'ubicacion_salida'], variables['archivo_configuraciones']
    procesar(ubicacion_entrada, ubicacion_salida, archivo_configuraciones)
