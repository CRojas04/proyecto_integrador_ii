# Funciones para el ETL
import numpy as np
import openpyxl
import pandas as pd
import unidecode
from stdnum.co import nit

# Tipos soportados (variable de entorno)
tipos = set(['1', '2', '4'])


def cargar_excel(ubicacion_archivo, columnas=None):
    '''
    Método que carga archivos de excel y devuelve un DataFrame.
    Permite no incurrir en el error de la libreria xlrd:

        AssertionError: assert 0 <= self.rowx < X12_MAX_ROWS

    En caso de que el archivo se pueda cargar con pandas directamente, es mejor hacerlo. Este método se toma su tiempo:

    Archivo 73Mb -> 5mins


    Parámetros
    -------------
    ubicacion_archivo : str
        String con la ubicación del archivo
    columnas : lista de str
        Lista con los nombres de las columnas que se quieren importar. Si es None, se incluirán todas las columnas.
        Valor por defecto: None


    Devuelve
    ------------
    pd.DataFrame
        DataFrame con la información del archivo excel.
    '''

    # Abre el archivo
    xlsx = openpyxl.load_workbook(ubicacion_archivo, data_only=True)

    # Selecciona las columnas
    datos = xlsx.active.rows

    # Valores a llenar
    encabezado = None
    datos_filas = []

    cargo_encabezado = False

    for fila in datos:
        l = np.array([celda.value for celda in fila])

        if not cargo_encabezado:
            # Carga Encabezado
            cargo_encabezado = True
            posiciones, encabezado = extraer_columnas(l, columnas)

        else:
            # Agrega los datos
            datos_filas.append(l[posiciones])

    df = pd.DataFrame(datos_filas, columns=encabezado)

    return df


def extraer_columnas(encabezado, columnas):
    '''
    Método que extare las columnas de las primera linea segun las columnas recibidas.
    Este es un método de apoyo para la lectura de los exceles
    '''

    # Toma todas las columnas
    if columnas is None:
        posiciones = [i for i in range(len(encabezado))]

    # Revisa que todos esten
    else:

        posiciones = []
        for col in columnas:
            # Extrae la posicion

            pos = np.where(col == np.array(encabezado))[0]
            if len(pos) == 0:
                raise ValueError(
                    'El archivo recibido no tiene la columna: {}'.format(col))

            if len(pos) > 1:
                raise ValueError(
                    'El archivo recibido tiene la columna: {} repetida {} veces'.format(col, len(pos)))

            posiciones.append(pos[0])

        encabezado = columnas

    return (posiciones, encabezado)


def limpiar_nombre_municipio(nombre):
    '''
    Método que limpia el nombre de un municipio.
    Quita las tildes y lo deja en formato de titulo

    '''

    nombre = str(nombre).lower()
    nombre = unidecode.unidecode(nombre)
    nombre = nombre.title()

    return nombre


def clean(x):
    try:
        val = int(x)
        return str(val)
    except ValueError:
        if pd.isnull(x):
            return None
        return str(x)


def complete(x):
    try:
        val = int(x)
        return '00' + str(val)
    except ValueError:
        if pd.isnull(x):
            return None
        return str(x)


def extraer_tipo(x):
    try:
        val = str(int(x))
        return val[0]
    except ValueError:
        return None


def es_numero(val):
    try:
        int(val)
        return True
    except ValueError:
        return False


def construir_nit_unico(nit1):
    '''
    Estandariza el nit y devuelve su certidumbre
    '''

    if nit.is_valid(nit1):
        return (nit.format(nit1), 0.9)
    # Se lo asigna
    else:
        nit1 += nit.calc_check_digit(nit1)
        if nit.is_valid(nit1):
            return (nit.format(nit1), 1)
        else:  # Muy Corto
            return (nit.format(nit1), 0)


def construir_nit(nit1, nit2=None):
    '''
    Construye el nit de un cliente con digito de verificacion.
    Este metodo utiliza la libreria stdnum.co para verificar si es o no
    un valor validor de NIT. Tambien devuelve la certidumbre que se tiene sobre el NIT.
    '''

    # Mira la consistencia numerica de los indicadores.
    # Deben ser numeros, de lo contrario seran convertidos a none.
    # La libreria stdum solo acepta caracteres

    nit1 = extraer_nit(nit1)

    nit2 = extraer_nit(nit2)

    if nit1 is None and nit2 is None:
        return None, 0

    # Ajusta para que el no nulo o el mas corto se encuentre en el nit1
    if nit1 is None or (nit2 is not None and len(nit2) < len(nit1)):
        nit1, nit2 = nit2, nit1

    # Caso solo un nit
    if nit2 is None or nit1 == nit2:
        return construir_nit_unico(nit1)

    # Caso Dos Nits
    # ---------------
    # Caso ideales

    # Nit 2 tiene el digito y es igual a nit1
    if nit2[0:-1] == nit1:
        # Verifica los códigos de verificación de ambos nits y asigna certidumbre
        return verificar_codigo_nits(nit1, nit2)

    # Casos no tan ideales
    # Le da prioridad al primero
    if nit.is_valid(nit1):
        return nit.format(nit1), 0.9 * 0.5
    # Se lo asigna
    else:
        nit1 += nit.calc_check_digit(nit1)
        if nit.is_valid(nit1):
            return nit.format(nit1), 0.5
        else:  # Muy Corto
            return nit.format(nit1), 0


def verificar_codigo_nits(nit1, nit2):
    # El digito de verificación es correcto
    if nit.is_valid(nit2):
        # si el nit1 tambien es valido
        if nit.is_valid(nit1):
            return nit.format(nit2), 0.99
        else:
            return nit.format(nit2), 1
    else:
        nit1 += nit.calc_check_digit(nit1)
        if nit.is_valid(nit1):
            return nit.format(nit1), 0.9
        else:  # Muy Corto
            return nit.format(nit1), 0


def extraer_nit(nit_inicial):
    try:
        return str(np.abs(int(str(nit_inicial)))).strip()
    except ValueError:
        return None


def agregar_indice_transparencia(file):
    '''
    Consolida los indices de transparencia departamentales y municipales.
    '''
    itd = file.it_x
    ita = file.it_y

    if ita is not np.nan:
        return ita
    elif itd is not np.nan:
        return itd
    else:
        return np.nan
