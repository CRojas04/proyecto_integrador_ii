# Funciones para la lectura de parametros desde consola
import optparse
import getopt
import os
import sys

USAGE_STRING = '*.py -i <carpeta_de_entrada> -o <carpeta_salida> [-c <archivo_configuraciones>]'


def extraer_parametros():
    '''
    Método que extrae los parametros de ejecución y crea la carpeta de salida
    en caso de que no exista
    '''

    variables = {}
    variables['ubicacion_entrada'] = None
    variables['ubicacion_salida'] = None
    variables['archivo_configuraciones'] = None

    # Extrae los parametros
    try:
        opts, args = getopt.getopt(sys.argv[1:], "hi:o:c:", [
            "ifolder=", "ofolder=", "config="])
    except getopt.GetoptError:
        print('%s' % USAGE_STRING)
        sys.exit(2)
    for opt, arg in opts:
        if opt == '-h':
            print('Uso del script:')
            print(USAGE_STRING)
            sys.exit()

        elif opt in ("-i", "--ifolder"):
            variables['ubicacion_entrada'] = arg
        elif opt in ("-o", "--ofolder"):
            variables['ubicacion_salida'] = arg
        elif opt in ("-c", "--config"):
            variables['archivo_configuraciones'] = arg

    if variables['ubicacion_entrada'] is None or variables['ubicacion_salida'] is None:
        print('Favor incluir ambos parametros obligatorios')
        print(USAGE_STRING)
        sys.exit(2)

    if not os.path.exists(variables['ubicacion_salida']):
        os.makedirs(variables['ubicacion_salida'])

    return (variables)
